#!/bin/bash
_os="`uname`"
_file="data/data.sql"
docker-compose exec db sh -c 'exec mysqldump "$MYSQL_DATABASE" -u "$MYSQL_USER" -p"$MYSQL_PASSWORD"' > $_file
if [[ "$_os" == "Darwin"* ]]; then
  sed -i '' 1,1d $_file
else
  sed -i 1,1d $_file
fi
