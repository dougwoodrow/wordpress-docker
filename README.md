# WPD - Wordpress Docker

#### Wordpress Development & Production environments utilizing Docker & Docker Compose

## Getting Started - Development

1. Clone repo and cd into dir
2. Configure `development.yml` with port numbers to not confliuct with any existing hosts
3. ​



## Common Concepts

```
-> app - the location of your Wordpress application
-> data - used to store and restore database dumps
-> conf - sample configuration files that may be used to assist with deployment
```

##### Starting containers

You can start the containers with the up command in daemon mode (by adding **-d** as a param) or by using the start command:

```
sudo docker-compose start
```

##### Stopping containers

```
sudo docker-compose stop
```

##### Remove containers

To stop and remove all the containers use the **down** command

```
sudo docker-compose down
```

... or the **rm** command if the containers are stopped already.

```
sudo docker-compose rm --all
```

##### Create DB Dump

When a DB dump is created, the next container that is started fresh will restore from this DB dump. The WP URL is explicitly set in the Docker environment in the Compose file so you don't need to worry about the URL in the DB dumps. 

```
sudo ./export.sh
```

This will dump a SQL file to `data/data.sql` that can be committed and tracked in git.

------

## Starting a new project

Make sure you have the latest versions of **Docker** and **Docker Compose** installed on your machine.

**On Mac**, install [Docker for Mac](https://docs.docker.com/docker-for-mac/install/).

**On Ubuntu** installations.

```
sudo apt-get install -y apt-transport-https ca-certificates curl software-properties-common && curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add - && sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" && sudo apt-get update && sudo apt-get install -y docker-ce
```

The above script adds the Docker repo to Ubuntu package repo and then installs Docker CE, which includes Docker Compose.

## Development Environments

##### Create containers

Open a terminal and *cd* to the folder you have the `development.yml` and configure:

##### Wordpress

      WP_HOME: http://127.0.0.1:4000
      WP_SITEURL: http://127.0.0.1:4000
      DB_PASSWORD: password (must match below password)
      DB_USER: wordpress (must match below user)
      DB_HOST: db
      DB_NAME: wordpress (must match below database)
##### Proxy

    ports:
      - "4000:80"
##### DB

      MYSQL_DATABASE: wordpress
      MYSQL_USER: wordpress
      MYSQL_PASSWORD: password
Then run:

```
sudo docker-compose up
```

The containers are now build and running. You should be able to access the Wordpress installation with the configured IP in the browser address. For convenience you may add a new entry into your hosts file.

## Opinionated Development

WPD takes an opinonated view of Wordpress Docker development and deployment, to be familiar with these opinions consider **these core philosophies:**

### Hosting with SSL

One of the key pivot points to building a Docker-ized deloyment was the ability to host with SSL-enabled containers, to make production deployment straightforward. The option to automatically use LetsEncrypt to get the containers with SSL quickly was unfromately not a very favorable option due to issues setting up certs within containers with Nginx configs. 

Rather, we prefer Infrastructure 

### Commit & version your WP to git